package ssm.error;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.StartupConstants.CSS_DIALOGBOX;
import static ssm.StartupConstants.CSS_ERROR;
import static ssm.StartupConstants.CSS_TANDCTEXT;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.view.SlideShowMakerView;

/**
 * This class provides error messages to the user when the occur. Note
 * that error messages should be retrieved from language-dependent XML files
 * and should have custom messages that are different depending on
 * the type of error so as to be informative concerning what went wrong.
 * 
 *  * @author McKilla Gorilla & Rahul Verma
 * id: 110134817
 */
public class ErrorHandler {
    // APP UI
    private SlideShowMakerView ui;
    
    // KEEP THE APP UI FOR LATER
    public ErrorHandler(SlideShowMakerView initUI) {
	ui = initUI;
    }
    
    /**
     * This method provides all error feedback. It gets the feedback text,
     * which changes depending on the type of error, and presents it to
     * the user in a dialog box.
     * 
     * @param errorType Identifies the type of error that happened, which
     * allows us to get and display different text for different errors.
     */
    public void processError(LanguagePropertyType errorType, String errorDialogTitle, String errorDialogMessage)
    {
        // GET THE FEEDBACK TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String errorFeedbackText = props.getProperty(errorType);
             
        // POP OPEN A DIALOG TO DISPLAY TO THE USER
        Stage errorStage = new Stage();
        HBox buttonBox = new HBox();
        
        Button closeAPP = new Button(props.getProperty(LanguagePropertyType.CLOSE_APP));
        Button cancel = new Button(props.getProperty(LanguagePropertyType.CANCEL));
        buttonBox.getChildren().addAll(closeAPP,cancel);
        buttonBox.setAlignment(Pos.CENTER);
        BorderPane borderPane = new BorderPane();
        Label errorMessage = new Label(errorDialogMessage);
        
        errorMessage.getStyleClass().add(CSS_ERROR);
        borderPane.setCenter(errorMessage);
        borderPane.setBottom(buttonBox);
        borderPane.getStyleClass().add(CSS_DIALOGBOX);
        Scene errorScene = new Scene(borderPane, 400,400);
        errorScene.getStylesheets().add(STYLE_SHEET_UI);
        closeAPP.setOnAction(e->{
        System.exit(0);
        });
        cancel.setOnAction(e->{
        errorStage.close();
        });
        errorStage.setScene(errorScene);
        errorStage.show();
       
    }    
}
