package ssm;

/**
 * This class provides the types that will be need to be loaded from
 * language-dependent XML files.
 * 
 * @author McKilla Gorilla & Rahul Verma
 * id: 110134817
 */
public enum LanguagePropertyType {
    // TITLE FOR WINDOW TITLE BAR
    // TITLE FOR WINDOW TITLE BAR
    TITLE_WINDOW,
    
    // APPLICATION TOOLTIPS FOR BUTTONS
    TOOLTIP_NEW_SLIDE_SHOW,
    TOOLTIP_LOAD_SLIDE_SHOW,
    TOOLTIP_SAVE_SLIDE_SHOW,
    TOOLTIP_VIEW_SLIDE_SHOW,
    TOOLTIP_EXIT,
    TOOLTIP_ADD_SLIDE,
    TOOLTIP_REMOVE_SLIDE,
    TOOLTIP_MOVE_UP,
    TOOLTIP_MOVE_DOWN,
    TOOLTIP_PREVIOUS_SLIDE,
    TOOLTIP_NEXT_SLIDE,
    
    // DEFAULT VALUES
    DEFAULT_IMAGE_CAPTION,
    DEFAULT_SLIDE_SHOW_TITLE,
    
    // UI LABELS
    LABEL_CAPTION,
    CLOSE_APP,
    CANCEL,
    
    SAVE,
    YES,
    NO,
    SAVE_PROMPT,
    
    
    /* THESE ARE FOR LANGUAGE-DEPENDENT ERROR HANDLING,
     LIKE FOR TEXT PUT INTO DIALOG BOXES TO NOTIFY
     THE USER WHEN AN ERROR HAS OCCURED */
    ERROR_DATA_FILE_LOADING,
    ERROR_PROPERTIES_FILE_LOADING,
    
    ERROR_SAVING,
    ERROR_SAVING_MESSAGE,
    
    ERROR_LOAD_SHOW,
    ERROR_LOAD_SHOW_MESSAGE,
    
    ERROR_NO_FILE_SELECTED,
    ERROR_NO_FILE_SELECTED_MESSAGE,
    
    ERROR_MISSING_IMAGE,
    ERROR_MISSING_IMAGE_MESSAGE,
    
    
    
    
    
    
    
    
    
    
    
}
